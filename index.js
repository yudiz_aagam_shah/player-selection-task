const data = require('./players.json')
const roleData = require('./player_roles.json')

let extractedValue = roleData.map(function(item) { return item['id'] });
console.log(extractedValue);

var teamId1Arr = []
var teamId31Arr = []
var role1Arr = []
var role2Arr = []
var role3Arr = []
var role4Arr = []
var playerRoleId1Arr = []
var playerRoleId2Arr = []
var playerRoleId3Arr = []
var playerRoleId4Arr = []
var playerTeamId31Arr = []
var teamArr = []

// Function which divides the Data into different arrays on basis of Player Role Id
function roleBasedArray() {
    for (var i = 0; i < data.length; i++) {
        if (data[i].player_role_id === 1) {
            role1Arr.push(data[i]);
        } else if (data[i].player_role_id === 2) {
            role2Arr.push(data[i]);
        } else if (data[i].player_role_id === 3) {
            role3Arr.push(data[i]);
        } else if (data[i].player_role_id === 4) {
            role4Arr.push(data[i]);
        }
    }
}

// Function calling ItSelf
roleBasedArray()

// Function which divides the Data into different arrays on basis of Player Team Id
function teamBasedArray() {
    for (var i = 0; i < data.length; i++) {
        if (data[i].team_id === 1) {
            teamId1Arr.push(data[i]);
        } else {
            teamId31Arr.push(data[i]);
        }
    }
}

// Function calling ItSelf
teamBasedArray()

// console.log("Player Role Id 1", role1Arr);
// console.log("Player Role Id 2", role2Arr);
// console.log("Player Role Id 3", role3Arr);
// console.log("Player Role Id 4", role4Arr);

// For Player Role Id 1
function getRandomEntryOfRoleId1() { // Function for picking the random player from Array of Role Id 1
    return role1Arr[Math.round(Math.random() * (role1Arr.length - 1))];
}

function entryExistsOfRoleId1(entryOfRoleId1) { // Function for checking the duplicate from array of Role ID 1
    return playerRoleId1Arr.indexOf(entryOfRoleId1) > -1;
}

for (var i = 0; i < 3; i++) {
    var entryOfRoleId1;

    do {

        entryOfRoleId1 = getRandomEntryOfRoleId1();

    } while (entryExistsOfRoleId1(entryOfRoleId1))

    playerRoleId1Arr.push(entryOfRoleId1); // Pushing into Array of Role Id 1
    // console.log(entry);
}

// console.log("Player Role 1", playerRoleId1Arr);

// For Player Role Id 2
function getRandomEntryOfRoleId2() { // Function for picking the random player from Array of Role Id 2
    return role2Arr[Math.round(Math.random() * (role2Arr.length - 1))];
}

function entryExistsOfRoleId2(entryOfRoleId2) { // Function for checking the duplicate from array of Role Id 2
    return playerRoleId2Arr.indexOf(entryOfRoleId2) > -1;
}

for (var i = 0; i < 4; i++) {
    var entryOfRoleId2;

    do {

        entryOfRoleId2 = getRandomEntryOfRoleId2();

    } while (entryExistsOfRoleId2(entryOfRoleId2))

    playerRoleId2Arr.push(entryOfRoleId2); // Pushing into Array of Role Id 2
    // console.log(entry);
}

// console.log("Player Role 2", playerRoleId2Arr);

// For Player Role Id 3
function getRandomEntryOfRoleId3() { // Function for picking the random player from Array of Role Id 3
    return role3Arr[Math.round(Math.random() * (role3Arr.length - 1))];
}

function entryExistsOfRoleId3(entryOfRoleId3) { // Function for checking the duplicate from array of Role Id 3
    return playerRoleId3Arr.indexOf(entryOfRoleId3) > -1;
}

for (var i = 0; i < 5; i++) {
    var entryOfRoleId3;

    do {

        entryOfRoleId3 = getRandomEntryOfRoleId3();

    } while (entryExistsOfRoleId3(entryOfRoleId3))

    playerRoleId3Arr.push(entryOfRoleId3); // Pushing into Array of Role Id 3
    // console.log(entry);
}

// console.log("Player Role 3", playerRoleId3Arr);

// For Player Role Id 4
function getRandomEntryOfRoleId4() { // Function for picking the random player from Array of Role Id 4
    return role4Arr[Math.round(Math.random() * (role4Arr.length - 1))];
}

function entryExistsOfRoleId4(entryOfRoleId4) { // Function for checking the duplicate from array of Role Id 4
    return playerRoleId4Arr.indexOf(entryOfRoleId4) > -1;
}

for (var i = 0; i < 5; i++) {
    var entryOfRoleId4;

    do {

        entryOfRoleId4 = getRandomEntryOfRoleId4();

    } while (entryExistsOfRoleId4(entryOfRoleId4))

    playerRoleId4Arr.push(entryOfRoleId4); // Pushing into Array of Role Id 4
    // console.log(entry);
}

// console.log("Player Role 4", playerRoleId4Arr);


var tempArr = [...playerRoleId1Arr, ...playerRoleId2Arr, ...playerRoleId3Arr, ...playerRoleId4Arr];
// console.log("TempArray", tempArr);

function getRandomEntryFinal() { // Function for picking the random player from Merge array of Array 1,2,3 and 4
    return tempArr[Math.round(Math.random() * (tempArr.length - 1))];
}

function entryExistsFinal(entryFinal) { // Function for checking the duplicate from Merge array of Array 1,2,3 and 4
    return teamArr.indexOf(entryFinal) > -1;
}

for (var i = 0; i < 11; i++) {
    var entryFinal;

    do {

        entryFinal = getRandomEntryFinal();

    } while (entryExistsFinal(entryFinal))

    teamArr.push(entryFinal); // Pushing into Merge array of Array 1,2,3 and 4 
    // console.log(entry);
}

// Count Of Player Role Id 1
const countOfPlayerRoleId1 = teamArr.filter(function(item) {
    if (item.player_role_id === 1) {
        return true;
    } else {
        return false;
    }
}).length;

// console.log("Count Of Player Id 1 =", countOfPlayerRoleId1);

// Count Of Player Role Id 2
const countOfPlayerRoleId2 = teamArr.filter(function(item) {
    if (item.player_role_id === 2) {
        return true;
    } else {
        return false;
    }
}).length;

// console.log("Count Of Player Id 2 =", countOfPlayerRoleId2);

// Count Of Player Role Id 3
const countOfPlayerRoleId3 = teamArr.filter(function(item) {
    if (item.player_role_id === 3) {
        return true;
    } else {
        return false;
    }
}).length;

// console.log("Count Of Player Id 3 =", countOfPlayerRoleId3);

// Count Of Player Role Id 4
const countOfPlayerRoleId4 = teamArr.filter(function(item) {
    if (item.player_role_id === 4) {
        return true;
    } else {
        return false;
    }
}).length;

// console.log("Count Of Player Id 4 =", countOfPlayerRoleId4);

// Conditions for fulfilling the Min and Max Players
if (countOfPlayerRoleId1 == 0) { // If role Id 1 is 0, then pick from the roleArray 1
    do {

        entryOfRoleId1 = getRandomEntryOfRoleId1();

    } while (entryExistsOfRoleId1(entryOfRoleId1))

    teamArr.push(entryOfRoleId1);
} else if (countOfPlayerRoleId1 > 1) { // If role Id 1 is >, delete and enter the random
    var removeIndex = teamArr.map(function(item) { return item.player_role_id; }).indexOf(1);

    teamArr.splice(removeIndex, 1);

    do {

        entryFinal = getRandomEntryFinal();

    } while (entryExistsFinal(entryFinal))

    teamArr.push(entryFinal);

    console.log("--------- Count Of 1");
} else if (countOfPlayerRoleId3 != 3 && countOfPlayerRoleId4 != 3) { // If Role Id 3 and 4 and not equal to 3, then enter the random
    var removeIndex = teamArr.map(function(item) { return item.player_role_id; }).indexOf(1);

    teamArr.splice(removeIndex, 1);

    do {

        entryOfRoleId3 = getRandomEntryOfRoleId3();

    } while (entryExistsOfRoleId3(entryOfRoleId3))

    do {

        entryOfRoleId4 = getRandomEntryOfRoleId4();

    } while (entryExistsOfRoleId4(entryOfRoleId4))

    teamArr.push(entryOfRoleId3, entryOfRoleId4);
    console.log("--------- Count Of 3");
}

// Doing the total of Credits of 11 Players
let total_credits = teamArr.reduce(function(accumulator, item) {
    return accumulator + item.player_credits;
}, 0);

console.log(total_credits);

// Checking If Credits is More than 100, Than remove and enter!
if (total_credits > 100) {
    teamArr.sort(function() { return 0.5 - Math.random(); }).pop();
    do {

        entryFinal = getRandomEntryFinal();

    } while (entryExistsFinal(entryFinal))

    teamArr.push(entryFinal);
}


function getRandomEntryOfTeamId31() { // Function for picking the random player from Array of Team Id 31
    return teamId31Arr[Math.round(Math.random() * (teamId31Arr.length - 1))];
}

function entryExistsOfTeamId31(entryOfTeamId31) { // Function for checking the duplicate from array of Team ID 31
    return playerTeamId31Arr.indexOf(entryOfTeamId31) > -1;
}

// Count Of Team Id 1
const countOfTeamId = teamArr.filter(function(item) {
    if (item.team_id === 1) {
        return true;
    } else {
        return false;
    }
}).length;

console.log(countOfTeamId);

// Condition If Count Of TeamId 1 is more than delete and enter
if (countOfTeamId > 7) {
    var removeIndex = teamArr.map(function(item) { return item.team_id; }).indexOf(1);

    teamArr.splice(removeIndex, 1);

    var entryOfTeamId31;

    do {

        entryOfTeamId31 = getRandomEntryOfTeamId31();

    } while (entryExistsOfTeamId31(entryOfTeamId31))

    teamArr.push(entryOfTeamId31);
}

// console.log("Before sum", teamArr);



console.log("Final Team", teamArr);